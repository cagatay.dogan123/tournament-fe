import { BrowserRouter, Route, Routes } from 'react-router-dom';
import RestrictedRoute from "./auth/RestrictedRoute";
import PrivateRoute from "./auth/PrivateRoute";
import Home from './components/Home';
import Login from './components/Login';
import TournamentManagement from './components/TournamentManagement';
import UserManagement from './components/UserManagement';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<PrivateRoute/>}>
          <Route path='/' element={<Home/>}/>
          <Route path='/management/tournament' element={<TournamentManagement/>}/>
          <Route path='/management/user' element={<UserManagement/>}/>
        </Route>
        <Route path='/login' element={<RestrictedRoute/>}>
          <Route path='/login' element={<Login/>}/>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;