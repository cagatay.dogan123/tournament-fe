import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const RestrictedRoute = (props) => {
  const token = localStorage.getItem('auth');
  // console.log("token", token);
  return <>{!token ? <Outlet /> : <Navigate to="/" />}</>;

};

export default RestrictedRoute;