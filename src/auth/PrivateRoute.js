import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const PrivateRoute = (props) => {
  const token = localStorage.getItem("auth");
  // console.log("token", token);
  return <>{token ? <Outlet /> : <Navigate to="/login" />}</>;
};

export default PrivateRoute;