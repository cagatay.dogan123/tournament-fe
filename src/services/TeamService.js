import axios from 'axios';

const TEAM_API_BASE_URL = "http://localhost:8080/api/v1/teams";

const TeamService = {

    getTeams: () => {
      return axios.get(TEAM_API_BASE_URL);
    },

    createTeam: (team) => {
      return axios.post(TEAM_API_BASE_URL, team);
    },

    getTeamById: (teamId) => {
      return axios.get(TEAM_API_BASE_URL + '/' + teamId);
    },

    updateTeam: (team, teamId) => {
      return axios.put(TEAM_API_BASE_URL + '/' + teamId, team);
    },

    deleteTeam: (teamId) => {
      return axios.delete(TEAM_API_BASE_URL + '/' + teamId);
    }
}

export default TeamService