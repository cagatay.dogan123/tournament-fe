import axios from 'axios';

const TOURNAMENT_API_BASE_URL = "http://localhost:8080/api/v1/tournaments";

const TournamentService = {

    getTournaments: () => {
      return axios.get(TOURNAMENT_API_BASE_URL);
    },

    createTournament: (tournament) => {
      return axios.post(TOURNAMENT_API_BASE_URL, tournament);
    },

    getTournamentById: (tournamentId) => {
      return axios.get(TOURNAMENT_API_BASE_URL + '/' + tournamentId);
    },

    updateTournament: (tournament, tournamentId) => {
      return axios.put(TOURNAMENT_API_BASE_URL + '/' + tournamentId, tournament);
    },

    deleteTournament: (tournamentId) => {
      return axios.delete(TOURNAMENT_API_BASE_URL + '/' + tournamentId);
    }
}

export default TournamentService