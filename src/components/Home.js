import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';

import Row from "../utils/Row";
import createData from "../hooks/createData";

const mockMatchInfo = [
  {id: 1, teamOne: 'Hello', teamTwo: 'World', score: '-'},
  {id: 2, teamOne: 'yo', teamTwo: 'what', score: '1-1'},
  {id: 3, teamOne: 'same', teamTwo: 'person', score: '2-3'},
  {id: 4, teamOne: 'yes', teamTwo: 'no', score: '1-4'},
]

const rows = [
  createData(1, 2022, 'Futbol', mockMatchInfo),
  createData(2, 2021, 'Basketbol', mockMatchInfo),
  createData(3, 2020, 'Voleybol', mockMatchInfo),
];

const Home = () => {
  const navigate = useNavigate();

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center"
        }}
      >
        <Typography component="h1" variant="h5" mb={2}>
          Turnuva
        </Typography>
        <Button variant="outlined" sx={{ mb: 2 }} onClick={() => {navigate("/management/tournament");}}>Turnuva Ekle</Button>
        <TableContainer component={Paper}>
          <Table aria-label="collapsible table">
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell align="right">Yıl</TableCell>
                <TableCell align="right">Turnuva Tipi</TableCell>
                <TableCell align="right">Turnuva Detayları</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <Row key={row.id} row={row} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Container>
  );
};

export default Home;