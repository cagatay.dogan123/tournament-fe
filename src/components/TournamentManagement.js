import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import EmployeeService from '../services/EmployeeService'

import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Stack from '@mui/material/Stack';

const types = [
  {id: 1, name: 'Futbol'},
  {id: 2, name: 'Basketbol'},
  {id: 3, name: 'Voleybol'}
]

const mockEmployees = [
  {id: 1, username: 'user1', role: "normal", playerNumber: "1", age: "43"},
  {id: 2, username: 'user2', role: "normal", playerNumber: "2", age: "23"},
  {id: 3, username: 'user3', role: "normal", playerNumber: "3", age: "36"},
  {id: 4, username: 'admin1', role: "admin", playerNumber: "4", age: "54"},
  {id: 5, username: 'admin2', role: "admin", playerNumber: "5", age: "27"},
  {id: 6, username: 'admin3', role: "admin", playerNumber: "6", age: "38"},
  {id: 7, username: 'leader1', role: "teamLeader", playerNumber: "7", age: "45"},
  {id: 8, username: 'leader2', role: "teamLeader", playerNumber: "8", age: "34"},
  {id: 9, username: 'leader3', role: "teamLeader", playerNumber: "9", age: "26"},
]

const TournamentManagement = () => {
  const navigate = useNavigate();
  const [employees, setEmployees] = useState([]);

  const [tournamentType, setTournamentType] = useState('');
  const [dialog, setDialog] = useState(false);
  const [numberOfPlayer, setNumberOfPlayer] = useState('');
  const [teams, setTeams] = useState([]);
  const [teamName, setTeamName] = useState('');
  const [teamLeader, setTeamLeader] = useState('');
  const [selectedPlayers, setSelectedPlayers] = useState([]);

  useEffect(() => {
    // EmployeeService.getEmployees().then((res) => { setEmployees(res.data); });
    setEmployees(mockEmployees);
  }, []);

  const addTournament = () => {
    console.log(tournamentType, teams);
  };

  const handlePlayerSelect = (player, index) => {
    let newArr = [...selectedPlayers];
    newArr[index] = player;
    setSelectedPlayers(newArr);
  };

  const addTeam = () => {
    console.log(teamName);
    console.log(teamLeader);
    console.log(selectedPlayers);
    setTeams([...teams, {name: teamName, leader: teamLeader, players: selectedPlayers}]);
    clearDialog();
  };

  const clearDialog = () => {
    setTeamName('');
    setTeamLeader('');
    setSelectedPlayers([]);
    setDialog(false);
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center"
        }}
      >
        <Typography component="h1" variant="h5">
          Turnuva Yönetimi
        </Typography>
        <FormControl fullWidth margin="dense">
          <InputLabel id="type-label">Turnuva Tipi</InputLabel>
          <Select
            labelId="type-label"
            id="type"
            value={tournamentType}
            label="Turnuva Tipi"
            onChange={(event) => setTournamentType(event.target.value)}
          >
            {types.map((item) => (
              <MenuItem
                key={item.id}
                value={item.id}
              >
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <TextField
          disabled={teams.length > 0}
          fullWidth 
          margin="dense"
          id="numberOfPlayer"
          label="Takımlardaki Oyuncu Sayısı"
          type="number"
          value={numberOfPlayer}
          onChange={(event) => setNumberOfPlayer(event.target.value)}
        />
        <Button disabled={!numberOfPlayer || numberOfPlayer === '0'} variant="outlined" sx={{ mt: 2, mb: 2 }} onClick={() => setDialog(true)}>Takım Ekle</Button>
        <Dialog fullWidth maxWidth="sm" open={dialog} onClose={clearDialog}>
          <DialogTitle>Takım Ekle</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              fullWidth
              margin="dense"
              id="name"
              label="Takım İsmi"
              value={teamName}
              onChange={(event) => setTeamName(event.target.value)}
            />
            <FormControl fullWidth margin="dense">
              <InputLabel id="teamLeader-label">Takım Sorumlusu</InputLabel>
              <Select
                labelId="teamLeader-label"
                id="teamLeader"
                value={teamLeader}
                label="Takım Sorumlusu"
                onChange={(event) => setTeamLeader(event.target.value)}
              >
                {employees.map((item) => (
                  item.role === "teamLeader" ?
                  <MenuItem
                    key={item.id}
                    value={item}
                  >
                    {item.username}
                  </MenuItem>
                  : null
                ))}
              </Select>
            </FormControl>
            {[...Array(Number(numberOfPlayer))].map((e, i) => (
              <FormControl fullWidth margin="dense" key={i}>
                <InputLabel id={i.toString()}>Oyuncu {i + 1}</InputLabel>
                <Select
                  labelId={i.toString()}
                  id="player"
                  value={selectedPlayers[i] ?? ''}
                  label="Oyuncu 0"
                  onChange={(event) => handlePlayerSelect(event.target.value, i)}
                >
                  {employees.map((item) => (
                    item.role === "normal" || item.role === "teamLeader" ?
                    <MenuItem
                      key={item.id}
                      value={item}
                    >
                      {item.username}, Numara: {item.playerNumber}, Yaş: {item.age}
                    </MenuItem>
                    : null
                  ))}
                </Select>
              </FormControl>
            ))}
          </DialogContent>
          <DialogActions>
            <Button onClick={clearDialog}>Vazgeç</Button>
            <Button disabled={!teamName || !teamLeader || selectedPlayers.length != numberOfPlayer} onClick={addTeam}>Ekle</Button>
          </DialogActions>
        </Dialog>
        <Stack direction="row" spacing={2} sx={{ mb: 2 }}>
        {teams.map((team, index) => (
          <Card sx={{ minWidth: 275 }} key={index}>
            <CardContent>
            <Typography variant="h5" component="div">
                Takım ismi: {team?.name}
              </Typography>
              <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                Takım sorumlusu: {team?.leader.username}
              </Typography>
              {team?.players.map((player, i) => (
                <Typography variant="body2" key={i}>
                  Oyuncu {i+1}: {player?.username}
                </Typography>
              ))}
            </CardContent>
            <CardActions>
              <Button size="small" color="error">Takımı Kaldır</Button>
            </CardActions>
          </Card>
        ))}
        </Stack>
        <Button variant="outlined" disabled={teams.length < 2 || !tournamentType} onClick={addTournament}>Turnuva Ekle</Button>
      </Box>
    </Container>
  )
};

export default TournamentManagement;