function createData(id, year, type, data) {
  return {
    id,
    year,
    type,
    data,
  };
}

export default createData;